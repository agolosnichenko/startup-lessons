class CreateCompletedTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :completed_tasks do |t|
      t.references :task, index: true
      t.references :user, index: true

      t.timestamps null:false
    end
    add_foreign_key :completed_tasks, :tasks
    add_foreign_key :completed_tasks, :users
    add_index :completed_tasks, [:task_id, :user_id], :unique => true
  end
end
