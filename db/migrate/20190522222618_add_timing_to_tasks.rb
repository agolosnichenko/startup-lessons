class AddTimingToTasks < ActiveRecord::Migration[5.2]
  def change
    add_column :tasks, :timing, :string
  end
end
