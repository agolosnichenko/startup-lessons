class AddDefaultPlan < ActiveRecord::Migration[5.2]
  def change
    change_column :users, :plan, :string, default: "free"
  end
end
