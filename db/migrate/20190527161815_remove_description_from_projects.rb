class RemoveDescriptionFromProjects < ActiveRecord::Migration[5.2]
  def change
    remove_column :projects, :whoitsfor
    remove_column :projects, :skills
    remove_column :projects, :features
    remove_column :projects, :requirement
  end
end
