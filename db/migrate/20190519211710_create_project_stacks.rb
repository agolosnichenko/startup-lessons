class CreateProjectStacks < ActiveRecord::Migration[5.2]
  def change
    create_table :project_stacks do |t|
      t.string :title
      t.references :project, foreign_key: true
    end
  end
end
