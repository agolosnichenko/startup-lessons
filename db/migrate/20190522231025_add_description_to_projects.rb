class AddDescriptionToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :whoitsfor, :string
    add_column :projects, :skills, :string
    add_column :projects, :features, :string
    add_column :projects, :requirement, :string
    rename_column :projects, :content, :about
  end
end
