class CreatePlanSubscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :plan_subscriptions do |t|
      t.references :user, index: true
      t.timestamps null:false
    end
    add_foreign_key :plan_subscriptions, :users
  end
end
