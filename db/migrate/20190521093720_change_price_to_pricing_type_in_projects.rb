class ChangePriceToPricingTypeInProjects < ActiveRecord::Migration[5.2]
  def change
    change_column :projects, :price, :string
    rename_column :projects, :price, :pricing_type
  end
end
