class Post < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  validates :title, presence: true, length: {maximum: 100}
  validates :note, presence: true, length: {maximum: 20000}
  validates :timing, presence: true

  has_attached_file :image
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  def shortnote
    note.length > 170? note[0..170] + " [...]" : note
  end
end
