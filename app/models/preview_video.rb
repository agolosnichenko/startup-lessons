class PreviewVideo < ApplicationRecord
  belongs_to :project
  validates :video, presence: true
end
