class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  after_create :send_notification
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :omniauthable, omniauth_providers: [:google_oauth2, :facebook, :github, :vkontakte]
  validates :name, presence: true, length: {maximum: 25}

  has_many :subscriptions
  has_many :projects, through: :subscriptions
  has_many :reviews
  has_many :completed_tasks

  def send_notification
    MyMailer.new_user(self).deliver
  end

  def self.from_google_oauth2(access_token)
    data = access_token.info
    user = User.where(:provider => access_token.provider, :uid => access_token.uid).first

    if user
      return user
    else
      registered_user = User.where(:email => data['email']).first
      if registered_user
        return registered_user
      else
        user = User.create(name: data['name'],
                provider: access_token.provider,
                uid: access_token.uid,
                email: data['email'],
                image: data['image'],
                password: Devise.friendly_token[0,20],
                plan: 'free'
             )
      end
    end
  end

  def self.from_facebook_oauth(access_token)
    data = access_token.info
    user = User.where(:provider => access_token.provider, :uid => access_token.uid).first

    if user
      return user
    else
      registered_user = User.where(:email => data['email']).first
      if registered_user
        return registered_user
      else
        user = User.create(name: data['name'],
                provider: access_token.provider,
                uid: access_token.uid,
                email: data['email'],
                image: data['image'],
                password: Devise.friendly_token[0,20],
                plan: 'free'
             )
      end
    end
  end

  def self.from_github_oauth(access_token)
    data = access_token.info
    user = User.where(:provider => access_token.provider, :uid => access_token.uid).first

    if user
      return user
    else
      registered_user = User.where(:email => data['email']).first
      if registered_user
        return registered_user
      else
        user = User.create(name: data['name'],
                provider: access_token.provider,
                uid: access_token.uid,
                email: data['email'],
                image: data['image'],
                password: Devise.friendly_token[0,20],
                plan: 'free'
             )
      end
    end
  end

  def self.from_vkontakte_oauth(access_token)
    data = access_token.info
    user = User.where(:provider => access_token.provider, :uid => access_token.uid).first

    if user
      return user
    else
      registered_user = User.where(:email => access_token.uid + "@vkuserlogin.com").first
      if registered_user
        return registered_user
      else
        user = User.create(name: data['name'],
                provider: access_token.provider,
                uid: access_token.uid,
                email: access_token.uid + "@vkuserlogin.com",
                image: data['image'],
                password: Devise.friendly_token[0,20],
                plan: 'free'
             )
      end
    end
  end

  # creates a new selection pick with selection_id and user_id
  def complete!(task)
    self.completed_tasks.create!(task_id: task.id)
  end
  # destroys a selection pick with matching selection_id and user_id
  def uncomplete!(task)
    completed_task = self.completed_tasks.find_by_task_id(task.id)
    completed_task.destroy!
  end
  # returns true if recipe selection is already picked by user
  def completed?(task)
    self.completed_tasks.find_by_task_id(task.id)
  end

end
