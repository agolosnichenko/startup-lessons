class Project < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  has_many :tasks
  has_many :subscriptions
  has_many :users, through: :subscriptions
  has_many :reviews
  has_many :project_stacks
  has_many :preview_videos

  validates :name, presence: true, length: {maximum: 100}
  validates :about, presence: true, length: {maximum: 5000}
  validates :pricing_type, presence: true
  validates :status, presence: true
  validates :duration, presence: true, numericality: {only_integer: true}

  has_attached_file :image, styles: { tiny: "210", thumb: "358" }, convert_options: { all: "-quality 100" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  def shortname
    name.length > 25? name[0..25] + "..." : name
  end

  def average_rating
    reviews.blank? ? 0 : reviews.average(:star).round(2)
  end

  #def price_in_cents
  #  price*100
  #end

end
