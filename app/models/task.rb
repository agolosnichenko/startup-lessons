class Task < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  belongs_to :project
  has_many :completed_tasks

  validates :title, presence: true, length: {maximum: 80}
  validates :project, presence: true

  def next
    project.tasks.where("tag > ? AND header = ?", tag, false).order(:tag).first
  end

  def prev
    project.tasks.where("tag < ? AND header = ?", tag, false).order(:tag).last
  end
end
