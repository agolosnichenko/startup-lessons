module ApplicationHelper

  require 'redcarpet'
  require 'rouge'
  require 'rouge/plugins/redcarpet'

  def avatar_url(user)
    gravatar_id = Digest::MD5::hexdigest(user.email).downcase
    if user.image
      user.image
    else
      "https://www.gravatar.com/avatar/#{gravatar_id}.jpg?d=identicon&s=50"
    end
  end

  def toastr_flash_class(type)
    case type
    when "alert"
      "toastr.error"
    when "notice"
      "toastr.success"
    else
      "toastr.info"
    end
  end

  class CustomRender < Redcarpet::Render::HTML
    include Rouge::Plugins::Redcarpet
  end

  def markdown(text)

    options = {
      :fenced_code_blocks => true,
      :no_intra_emphasis => true,
      :autolink => true,
      :strikethrough => true,
      :lax_html_blocks => true,
      :superscript => true,
      :underline => true,
      :highlight => true
    }

    render_options = {
        escape_html: true,
        hard_wrap: true,
        link_attributes: { rel: 'nofollow' },
        prettify: true
    }

    renderer = CustomRender.new(render_options)

    markdown_to_html = Redcarpet::Markdown.new(renderer, options)
    raw markdown_to_html.render(text)
  end

  def markdown_with_html(text)

    markdown_to_html = Redcarpet::Markdown.new(Redcarpet::Render::HTML, extensions = {})
    return markdown_to_html.render(text).html_safe
  end

  def timeago(time, options={})
    options[:class] ||= "timeago"
    content_tag(
      :time,
      time.to_s,
      options.merge(datetime: time.getutc.iso8601)
    ) if time
  end

  def getSubscriptionInfo(userId)
    userId = User.find(userId)
    subscription = if userId.stripe_subscription_id?
                 Stripe::Subscription.retrieve(userId.stripe_subscription_id)
               end
    return subscription
  end

end
