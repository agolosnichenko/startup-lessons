class PagesController < ApplicationController
  def pricing
  end

  def terms
  end

  def privacy
  end

  def payments
  end

  def about
  end

end
