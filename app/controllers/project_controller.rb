class ProjectController < ApplicationController
  def index
    @projects = Project.all
  end

  def show
    @project = Project.find(params[:id])
    @tasks = @project.tasks.order(:tag)
    @preview_videos = @project.preview_videos
    @status = @project.status

    if @status != 'active'
      redirect_to root_path
    end

    @joined = false

    if !current_user.nil? && !current_user.projects.nil?
      @joined = current_user.projects.include?(@project)
    end

    @users = @project.users.order('created_at desc').first(10)
    @review = Review.new
    @reviews = @project.reviews
    @hasReview = @reviews.find_by(user_id: current_user.id) if current_user
  end

  def list
    if !current_user.nil?
      @projects = current_user.projects
    end
  end

  def subscribe
    if !current_user.nil?

      project = Project.find(params[:project_id])

      if project.pricing_type == 'free'
        current_user.subscriptions.create(project: project)
        redirect_to project
      else
        if current_user.plan == 'free'
          redirect_to pricing_path, alert: "Приобретите подписку для доступа к этому проекту"
        else
          plan_subscription = PlanSubscription.find_by(user_id: current_user.id)
          allowedprojects = (((Time.current - plan_subscription.updated_at)/1.day).round)/30+1
          if (current_user.plan == 'standart' && current_user.projects.where("pricing_type == 'pro'").count >= allowedprojects)
            redirect_to pricing_path, alert: "Разрешенное число проектов - #{allowedprojects}. Улучшите тарифный план для доступа к этому проекту"
          else
            current_user.subscriptions.create(project: project)
            redirect_to project
          end
        end
      end
    end
  end

end
