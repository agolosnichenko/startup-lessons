class TaskController < ApplicationController
  before_action :authenticate_user!
  def show
    project = Project.find(params[:project_id])
    @tasks = project.tasks.order(:tag)

    joined = false

    if !current_user.nil? && !current_user.projects.nil?
      joined = current_user.projects.include?(project)
    end

    if !current_user.nil?

      if project.pricing_type == 'free'
        if joined

          @task = @tasks.find(params[:id])

          @next_task = @task.next
          @prev_task = @task.prev
        else
          flash[:notice] = "Ошибка доступа"
          redirect_to project
        end
      else
        if current_user.plan == 'free'
          redirect_to pricing_path, alert: "Приобретите подписку для доступа к этому проекту"
        else
          plan_subscription = PlanSubscription.find_by(user_id: current_user.id)
          allowedprojects = (((Time.current - plan_subscription.updated_at)/1.day).round)/30+1
          if (current_user.plan == 'standart' && current_user.projects.where("pricing_type == 'pro'").count > allowedprojects)
            redirect_to pricing_path, alert: "Улучшите тарифный план для доступа к этому проекту. Сейчас вам доступно "+allowedprojects.to_s+" проектов"
          else
            if joined

              @task = @tasks.find(params[:id])

              @next_task = @task.next
              @prev_task = @task.prev
            else
              flash[:notice] = "Ошибка доступа"
              redirect_to project
            end
          end
        end
      end

    end
  end

  def show_next
    @task = Task.find(params[:id])
    @project = Project.find(@task.project_id)
    joined = false

    if !current_user.nil? && !current_user.projects.nil?
      joined = current_user.projects.include?(@project)
    end

    if joined
      @next_task = @task.next
      @prev_task = @task.prev
      if current_user.completed?(@task)
        redirect_to [@next_task.project, @next_task]
      else
        current_user.complete!(@task)
        redirect_to [@next_task.project, @next_task]
      end
    else
      flash[:notice] = "Ошибка доступа"
      redirect_to @project
    end
  end

  def show_prev
    @task = Task.find(params[:id])
    @project = Project.find(@task.project_id)
    joined = false

    if !current_user.nil? && !current_user.projects.nil?
      joined = current_user.projects.include?(@project)
    end

    if joined

      @next_task = @task.next
      @prev_task = @task.prev
      if current_user.completed?(@task)
        redirect_to [@prev_task.project, @prev_task]
      else
        current_user.complete!(@task)
        redirect_to [@prev_task.project, @prev_task]
      end
    else
      flash[:notice] = "Ошибка доступа"
      redirect_to @project
    end
  end

end
