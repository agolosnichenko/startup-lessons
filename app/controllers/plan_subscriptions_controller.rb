class PlanSubscriptionsController < ApplicationController
  before_action :authenticate_user!
  def new
    @plan = params[:plan]
  end

  def create
    customer = if current_user.stripe_user_id?
                 Stripe::Customer.retrieve(current_user.stripe_user_id)
               else
                 Stripe::Customer.create(
                   :email => current_user.email,
                   :source => params[:stripeToken]
                 )
               end
    subscription = customer.subscriptions.create(
      plan: params[:plan] + params[:plan_period]
    )
    options = {
      stripe_user_id: customer.id,
      stripe_subscription_id: subscription.id,
      plan: params[:plan],
    }
    current_user.update(options)
    subscription = PlanSubscription.create user_id: current_user.id
    redirect_to root_path, notice: "Подписка оформлена успешно"
    #UserMailer.welcome_email(current_user.id).deliver

  end

  def update
    @plan = params[:plan]
  end

  def destroy
    customer = Stripe::Customer.retrieve(current_user.stripe_user_id)
    subscription = Stripe::Subscription.retrieve(current_user.stripe_subscription_id)
    subscription.delete()
    subscription = PlanSubscription.where(:user_id == current_user.id).delete_all
    current_user.update(stripe_subscription_id: nil)
    @plan = params[:plan]
    current_user.update(plan: 'free')
    user = current_user
    redirect_to root_path, notice: "Подписка отменена"
  end

  def update_card

    customer = Stripe::Customer.retrieve(current_user.stripe_user_id)
    customer.source = params[:stripeToken]
    customer.save
    redirect_to edit_user_registration_path, notice: "Данные банковской карты обновлены"
  end

end
