class CompletedTaskController < ApplicationController
  respond_to :js
  def complete
    @user = current_user
    @task = Task.find(params[:task_id])
    @project = Project.find(@task.project_id)
    @user.complete!(@task)
        respond_to do |format|
          format.html { redirect_to :back }
          format.js { render layout: false }
      end
  end

  def uncomplete
    #@completed_task = CompletedTask.where(":task_id == params[:task_id] and :user_id == current_user.id")
    @user = current_user
    @task = Task.find(params[:task_id])
    @project = Project.find(@task.project_id)
    @completed_task = @user.completed_tasks.find_by_task_id(params[:task_id])
    @completed_task.destroy
        respond_to do |format|
          format.html { redirect_to :back }
          format.js { render layout: false }
      end
  end
end
