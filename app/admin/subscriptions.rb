ActiveAdmin.register Subscription do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  permit_params :user_id, :project_id

  index do

    column :user
    column :project
    actions
  end

  form do |f|
    f.inputs do
      input :project, label: "Проект"
      input :user, label: "Пользователь"

    end
    actions
  end

end
