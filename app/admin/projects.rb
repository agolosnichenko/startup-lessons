ActiveAdmin.register Project do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  permit_params :name, :about, :pricing_type, :duration, :image, :video, :status

  show do |t|
    attributes_table do
      row :status, label: "Статус"
      row :name, label: "Название"
      row :about, label: "Описание"
      row :pricing_type, label: "Стоимость"
      row :duration, label: "Длительность"
      row :video, label: "Видео"
      row :image, label: "Изображение" do
        project.image? ? image_tag(project.image.url, height: '100') : content_tag(:span, "No photo yet")
      end
    end
  end

  form :html => {:enctype => "multipart/form-data"} do |f|
    f.inputs do
      f.input :status, label: "Статус", :as => :select, :collection => ['draft','inactive','active']
      f.input :name, label: "Название"
      f.input :about, label: "Описание"
      f.input :pricing_type, label: "Стоимость", :as => :select, :collection => ['pro','free']
      f.input :duration, label: "Длительность"
      f.input :video, label: "Видео"
      f.input :image, label: "Изображение", hint: f.project.image? ? image_tag(project.image.url, height: '100') : content_tag(:span, "Upload JPG/PNG/GIF image")
    end
    f.actions
  end

end
