ActiveAdmin.register Task do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  permit_params :title, :note, :video, :header, :tag, :project_id, :timing
  sortable tree: false, sorting_attribute: :tag
  index :as => :sortable do
    label :title
    actions
  end
  index do
    selectable_column
    column :header, label: "Заголовок?"
    column :title, label: "Заголовок"
    column :tag, label: "Тег"
    column :project, label: "Проект"
    column :timing, label: "Длительность"
    actions
  end

  form do |f|
    f.inputs do
      input :project, label: "Проект"
      input :title, label: "Заголовок"
      input :note, label: "Описание задания"
      input :timing, label: "Длительность"
      input :video, label: "Видео"
      input :header, label: "Заголовок?"

    end
    actions
  end

end
