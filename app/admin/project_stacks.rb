ActiveAdmin.register ProjectStack do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  permit_params :title, :project_id

  index do

    column :title
    column :project
    actions
  end

  form do |f|
    f.inputs do
      input :project, label: "Проект"
      input :title, label: "Стэк"

    end
    actions
  end

end
