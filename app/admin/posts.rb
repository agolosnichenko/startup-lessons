ActiveAdmin.register Post do
  permit_params :title, :note, :timing, :image

  show do |t|
    attributes_table do
      row :title
      row :note
      row :timing
      row :image do
        post.image? ? image_tag(post.image.url, height: '100') : content_tag(:span, "No photo yet")
      end
    end
  end

  form :html => {:enctype => "multipart/form-data"} do |f|
    f.inputs do
      f.input :title, label: "Заголовок"
      f.input :note, label: "Текст"
      f.input :timing, label: "Длительность"
      f.input :image, label: "Изображение", hint: f.post.image? ? image_tag(post.image.url, height: '100') : content_tag(:span, "Upload JPG/PNG/GIF image")
    end
    f.actions
  end
end
