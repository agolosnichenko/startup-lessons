class MyMailer < ActionMailer::Base
  require 'sendgrid-ruby'
  include SendGrid
  require 'json'

  def new_user(user)
    from = Email.new(email: 'no-reply@startup-lessons.ru')
    subject = 'Добро пожаловать на StartupLessons!'
    to = Email.new(email: user.email)
    content = Content.new(type: 'text/plain', value: 'Welcome!')
    mail = SendGrid::Mail.new(from, subject, to, content)
    # puts JSON.pretty_generate(mail.to_json)
    puts mail.to_json

    sg = SendGrid::API.new(api_key: ENV['MAILER_API_KEY'], host: 'https://api.sendgrid.com')
    response = sg.client.mail._('send').post(request_body: mail.to_json)
    puts response.status_code
    puts response.body
    puts response.headers
  end
end
