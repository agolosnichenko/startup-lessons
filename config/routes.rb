Rails.application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users, :controllers => { :omniauth_callbacks => 'users/omniauth_callbacks' }, :path => '', :path_names => {:sign_in => 'login', :sign_out => 'logout', :edit => 'profile'}

  get 'pages/terms'
  get 'pages/privacy'
  get 'pages/payments'
  get 'pages/about'
  get '/myprojects' => 'project#list'
  get '/pricing' => 'pages#pricing'
  post '/update_card' => 'plan_subscriptions#update_card'
  post '/subscribe' => 'project#subscribe'
  get '/blog' => 'post#index'

  get '/next' => 'task#show_next'
  get '/prev' => 'task#show_prev'

  match '/complete', to: 'completed_task#complete', via: :post
  match '/uncomplete', to: 'completed_task#uncomplete', via: :delete

  root 'project#index'

  resources :project do
    resources :task, only: [:show, :show_next, :show_prev]
  end

  resources :project do
    resources :project_stacks, only: [:show]
  end

  resources :project do
    resources :preview_videos, only: [:show]
  end

  resources :project do
    resources :reviews, only: [:create, :destroy]
  end

  resource :plan_subscriptions
  resources :post
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
